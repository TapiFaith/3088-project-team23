EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Assignment3.1-rescue:LTC1157CS8PBF-LTC1157CS8PBF-Assignment3.1-rescue U1
U 1 1 60BBE18C
P 4650 3200
AR Path="/60BBE18C" Ref="U1"  Part="1" 
AR Path="/60BBDBC4/60BBE18C" Ref="U1"  Part="1" 
F 0 "U1" H 5950 3565 50  0000 C CNN
F 1 "LTC1157CS8PBF" H 5950 3474 50  0000 C CNN
F 2 "lib:SO-8_S" H 4650 3200 50  0001 L BNN
F 3 "" H 4650 3200 50  0001 L BNN
F 4 "Linear Technology" H 4650 3200 50  0001 L BNN "VENDOR"
F 5 "ltc1157cs8#pbf" H 4650 3200 50  0001 L BNN "MANUFACTURER_PART_NUMBER"
	1    4650 3200
	1    0    0    -1  
$EndComp
$Comp
L Transistor_FET:Si4532DY Q2
U 1 1 60BBF6F3
P 8100 3300
F 0 "Q2" H 8305 3346 50  0000 L CNN
F 1 "Si4532DY" H 8305 3255 50  0000 L CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 8300 3225 50  0001 L CNN
F 3 "https://www.onsemi.com/pub/Collateral/SI4532DY-D.PDF" H 8200 3300 50  0001 L CNN
	1    8100 3300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R11
U 1 1 60BC1C45
P 8200 4250
F 0 "R11" H 8270 4296 50  0000 L CNN
F 1 "10" H 8270 4205 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 8130 4250 50  0001 C CNN
F 3 "~" H 8200 4250 50  0001 C CNN
	1    8200 4250
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:OPA188xxD U2
U 1 1 60BC2705
P 6600 4800
F 0 "U2" H 6944 4846 50  0000 L CNN
F 1 "OPA188xxD" H 6944 4755 50  0000 L CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 6500 4600 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/opa188.pdf" H 6750 4950 50  0001 C CNN
	1    6600 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	7250 3300 7900 3300
Wire Wire Line
	8200 3500 8200 4100
Wire Wire Line
	4650 3400 4500 3400
Wire Wire Line
	7250 3400 7600 3400
Wire Wire Line
	7600 3400 7600 3100
Wire Wire Line
	7600 3100 8200 3100
Wire Wire Line
	6300 4900 6000 4900
Wire Wire Line
	6900 4800 7250 4800
Wire Wire Line
	7250 4800 7250 3500
Text HLabel 7900 6150 0    50   Output ~ 0
GPIO21
Wire Wire Line
	8200 6150 7900 6150
Text HLabel 5600 5850 0    50   Input ~ 0
GPIO20
Wire Wire Line
	6000 4900 6000 5850
Wire Wire Line
	6000 5850 5600 5850
Text HLabel 4950 4850 0    50   Input ~ 0
GPIO16
Wire Wire Line
	6300 4700 5000 4700
Wire Wire Line
	5000 4700 5000 4850
Wire Wire Line
	5000 4850 4950 4850
Text HLabel 7150 5300 0    50   Output ~ 0
GPIO5
Wire Wire Line
	7250 4800 7750 4800
Wire Wire Line
	7750 4800 7750 5300
Wire Wire Line
	7750 5300 7150 5300
Connection ~ 7250 4800
NoConn ~ 4650 3300
Text GLabel 4500 5500 3    50   Input ~ 0
GND
Wire Wire Line
	4500 3400 4500 5300
Text HLabel 4050 3500 0    50   Input ~ 0
GPIO19
Wire Wire Line
	8200 4400 8200 6150
Wire Wire Line
	6500 5100 6500 5300
Wire Wire Line
	6500 5300 4500 5300
Connection ~ 4500 5300
Wire Wire Line
	4500 5300 4500 5500
Wire Wire Line
	4050 3500 4650 3500
Text GLabel 6500 4500 1    50   Input ~ 0
3.3V
Text GLabel 7600 3650 3    50   Input ~ 0
3.3V
Wire Wire Line
	7600 3650 7600 3400
Connection ~ 7600 3400
$EndSCHEMATC
